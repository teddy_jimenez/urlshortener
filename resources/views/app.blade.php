<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <title>{{ env('APP_NAME', 'URL Shortener') }}</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">URL Shortener</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">Shortener <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://bitbucket.org/teddy_jimenez/urlshortener/src/master/" target="_blank">API Documentation</a>
            </li>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="row justify-content-center text-center">
        <div class="col-md-6 col-12 mt-5 mb-4">
            <h1><a href="/">Url Shortener</a></h1>
        </div>
    </div>
    <div class="row justify-content-center mb-5">
        <div class="col-md-8 col-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul class="my-0 py-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="col-md-8 col-12 text-center">
            @if (isset($output))
                <div class="alert alert-success">
                    <h4>{{ $output['message'] }}</h4>
                    <p>Access to <a href="{{ $output['result']['url'] }}"
                                    target="_blank">{{ $output['result']['url'] }}</a></p>
                    <p>Have a look to the stats <a href="{{ $output['result']['stats'] }}"
                                                   target="_blank">{{ $output['result']['url'] }}</a></p>
                </div>
            @endif
        </div>
        <div class="col-md-8 col-12">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{ route('url.store') }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for="url">URL</label>
                                <input type="text" class="form-control" id="url" name="url"
                                       placeholder="Provide a valid url">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Generate URL</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8 col-12">
            <h5 class="mb-4">Top 10 Performing URLs</h5>
        </div>
        <div class="col-md-8 col-12">
            @if (isset($top_urls))
                @foreach($top_urls as $url)
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><a href="{{ $url->url }}" target="_blank">{{ $url->url }}</a></h5>
                            <p class="card-text text-info"><small><a href="{{route('url.redirection', $url->code)}}" target="_blank">{{route('url.redirection', $url->code)}}</a></small></p>
                            <p class="card-text">Visits {{ number_format($url->visits) }}</p>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
