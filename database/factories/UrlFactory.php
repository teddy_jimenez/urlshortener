<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Url;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Url::class, function (Faker $faker) {
    return [
        'code' => slug_shortener($faker->url),
        'url' => $faker->url,
        'visits' => $faker->numberBetween(0, 10000)
    ];
});
