<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $table = 'urls';

    protected $fillable = ['code', 'url', 'visits', 'qty'];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $casts = [
        'url' => 'string',
        'visits' => 'integer',
    ];

   public static $rules = [
        'code' => 'string|unique:urls,code',
        'url' => 'required|url|active_url',
        'visits' => 'integer',
   ];

    public static $rulesOnStats = [
        'qty' => 'required|numeric'
    ];

    public function getRouteKeyName()
    {
        return 'code';
    }

}
