<?php


namespace App\Http\Controllers;

use App\Url;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UrlController extends Controller
{

    /**
     * Create a new URL
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function create()
    {
        $tops = Url::orderBy('visits', 'desc')->take(10)->get();
        return view('app')->with('top_urls', $tops);
    }

    /**
     * Store Url
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        //validate inputs
        $this->validate($request, Url::$rules);

        //get unique code for url
        $new_url = $this->applyUrlShortener($request->input('url'));

        //append code to request
        $request->request->add(['code' => $new_url]);

        //prevent fields injection
        $inputs = $request->except(['visits']);

        //save request
        $query = Url::create($inputs);

        $output = [
            'message' => 'Short Link has been generated',
            'result' => [
                'code' => $query->code,
                'url' => route('url.redirection', $query->code),
                'stats' => route('api.url.info', $query->code),
                'http_code' => 200
            ]
        ];

        if ($request->wantsJson()) {
            return response()->json($output, 201);
        }

        $tops = Url::orderBy('visits', 'desc')->take(10)->get();

        return view('app')->with('output', $output)->with('top_urls', $tops);

    }

    /**
     * Find code and Redirect to resource's url
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function find($id){

        $record = Url::where('code', $id)->first();

        if (!$record) return abort(404);

        $record->visits = $record->visits+1;
        $record->update();

        return redirect($record->url, 302);

    }

    /**
     * Find url by Code and get info
     * @param $id
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function getUrlInfo($id)
    {
        $record = Url::where('code', $id)->first();

        if (!$record) {

            $output = [
                'message' => 'Url has not been found',
                'error' => [
                    'url' => '',
                    'http_code' => 404
                ]
            ];

            return response()->json($output, 404);

        }

        return response()->json($record, 200);

    }

    /**
     * Get Top 100 Urls
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUrlTops()
    {
        $record = Url::orderBy('visits', 'desc')->take(100)->get();

        $counter = 1;

        $record = $record->map(function ($value) use (&$counter) {

           $output = [
               'url_position' => $counter,
               'code' => $value->code,
               'short_url' => route('url.redirection', $value->code),
               'target_url' => $value->url,
               'visits' => $value->visits
           ];

            $counter++;

            return $output;

        });

        return response()->json($record, 200);

    }

    /**
     * Encode parameter
     * @param $base_url
     * @return bool|string
     */
    public function applyUrlShortener($base_url)
    {
        return slug_shortener($base_url);
    }

}
