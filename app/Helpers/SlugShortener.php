<?php

if(!function_exists('slug_shortener')) {
    function slug_shortener($string, $start = 0, $limit = 10) {

        //generate a random string
        $random_value = mt_rand();

        //generate a unique string based on the provided value
        $unique_value = uniqid($random_value, true);

        //generate a hash based on the unique value.
        $sha_value = sha1($unique_value);

        //convert string to another base 32 to decrease possibility of duplication
        $base_convertion = base_convert($sha_value, 16, 36);

        $reduce_string = substr($base_convertion, $start, $limit);

        return $reduce_string;

    }
}
