<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('url', 'UrlController@store')->name('api.url.store');

Route::get('url/{id}', 'UrlController@getUrlInfo')->name('api.url.info');

Route::get('url/top/100', 'UrlController@getUrlTops')->name('api.url.tops');
