<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UrlController@create');
Route::post('/', 'UrlController@store')->name('url.store');

Route::get('/{id}',  'UrlController@find')->name('url.redirection');

Route::get('please/testme', function (){
    return slug_shortener('this is a test line');
});
