# URL Shortener

Url Shortener is a simple API allowing users to cut long urls and retrieve stats related to them.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

This project needs the essentials libraries required by laravel.
- PHP >= 7.1.3
- BCMath PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Composer (locally or globally)

### Installing

A step by step guideline is described in the next few lines so you can install smoothly using CLI interface, for operating systems without GUI interface.

1 - Clone the following public repository [Url Shortener](https://teddy_jimenez@bitbucket.org/teddy_jimenez/urlshortener.git "Url Shortener")
.
```
git clone https://teddy_jimenez@bitbucket.org/teddy_jimenez/urlshortener.git
```
2 - Set folder and files permissions
.
```
sudo chown -R www-data:www-data {project_folder}
sudo chmod -R 775 {project_folder}

cd {project_folder}

sudo chown -R $USER:www-data storage
sudo chown -R $USER:www-data bootstrap/cache

chmod -R 775 storage
chmod -R 775 bootstrap/cache
```

3 - Duplicate .env.example file and rename it to .env
.
```
cp .env.example .env
```

4 - Run Composer to install dependencies and generate a project key
```
composer install 
php artisan key:generate
```

5 - Set database credentials and the project url in the .env config file
```
APP_URL=http://shortener.caribehub.com

DB_DATABASE=url_shortener_dev
DB_USERNAME=root
DB_PASSWORD=1234
```

6 - Run database migration and optionally you may fill database with demo data.
```
php artisan migrate //create database tables.
php artisan db:seed //(optional) fill database with demo data
```

If every step has been completed you are ready to start.


## How to use

The following methods allows you to interact with the api using curl, postman or any tool you choose

### Reach target url from short url [GET]

#### http://shortener.caribehub.com/{code}

```

+ Redirection to target url with http code 302 to prevent direct redirection from browser.
        
```

### Create a new minified url [POST]

#### http://shortener.caribehub.com/api/url

```

+ Request (application/json)

    + Headers   
    
        Accept: application/json
                
    + Parameter
    
        {
            "url": "http://google.com.do"
        }
    
+ Response 201 (application/json)

    + Body

        {
          "message": "Short Link has been generated",
          "result": {
            "code": "5a6zvt868p",
            "url": "http:\/\/shortener.caribehub.com\/5a6zvt868p",
            "stats": "http:\/\/shortener.caribehub.com\/api\/url\/5a6zvt868p",
            "http_code": 200
          }
        }
        
        
```

### Retrieve 100 most visited url's [GET]

#### http://shortener.caribehub.com/api/url/top/100

```

+ Request (application/json)

    + Headers   
    
        Accept: application/json

    + Response 201 (application/json)

        + Body
    
            [
              {
                "url_position": 1,
                "code": "o21ogbbddn",
                "short_url": "http:\/\/urlshortener.test\/o21ogbbddn",
                "long_url": "http:\/\/luettgen.com\/",
                "visits": 10000
              },
              {
                "url_position": 2,
                "code": "kf3fz0w5km",
                "short_url": "http:\/\/urlshortener.test\/kf3fz0w5km",
                "long_url": "http:\/\/feeney.com\/tenetur-et-tempore-distinctio-autem-ipsam-quibusdam-quas",
                "visits": 10000
              },
              {
                "url_position": 3,
                "code": "lrzmmz9g24",
                "short_url": "http:\/\/urlshortener.test\/lrzmmz9g24",
                "long_url": "http:\/\/www.graham.com\/",
                "visits": 10000
              },
              {
                "url_position": 4,
                "code": "g4cswm2n6e",
                "short_url": "http:\/\/urlshortener.test\/g4cswm2n6e",
                "long_url": "http:\/\/zieme.com\/unde-et-architecto-accusamus-aut",
                "visits": 9999
              },
              {
                "url_position": 5,
                "code": "r1lle3jpmb",
                "short_url": "http:\/\/urlshortener.test\/r1lle3jpmb",
                "long_url": "https:\/\/wiza.com\/velit-deserunt-voluptates-temporibus-repellendus.html",
                "visits": 9999
              },
              
              ...
              
            ]
```

### Get minified url stats [GET]

#### http://shortener.caribehub.com/api/url/{code}

```

+ Request (application/json)

    + Headers   
    
        Accept: application/json
    
+ Response 201 (application/json)

    + Body

        {
          "id": 50008,
          "code": "bdyxlivhke",
          "url": "http:\/\/google.com.do",
          "visits": 9
        }
        
        
```
## About the URL minification process

The process of URL minification without deep or extensive cryptography procedures were done using 
native functions of the language ensuring not duplication on 10k records insertions per 5 second.

The whole process is coded in the following file

```
project_folder/app/Helpers/SlugShortener.php 
```

### The procedure is the following 

1 - We generate a random value using the [Mersenne Twister algoritm](https://www.geeksforgeeks.org/php-mt_rand-function/)
```
$random_value = mt_rand();
```
2 - Then we create a unique string based on the provided value using the PHP's function [Uniq ID](https://www.geeksforgeeks.org/php-uniqid-function/)
```
$unique_value = uniqid($random_value, true);
```
3 - Later we build a hash from an already existing one [Sha Function](https://www.geeksforgeeks.org/php-md5-sha1-hash-functions/)
```
$sha_value = sha1($unique_value);
```
4 - Before completing the process we transform the current value's base to a greater one, decreasing the possibility of duplication [Base Function](https://www.geeksforgeeks.org/php-base_convert-math-function/)
```
$base_convertion = base_convert($sha_value, 16, 36);
```
5 - Finally we reduce the string to a very short code and return it as a part of the new minified url. This process allows the product owner migrate the platform to another domain without painfull installatio or a loong migration process.
```
$reduce_string = substr($base_convertion, $start, $limit);
```

To recreate this hash code generation directly in the browser go to: 

https://shortener.caribehub.com/please/testme

## Authors

* **Teddy Jimenez** - [Caribe Hub](https://caribehub.com/)

